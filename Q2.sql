USE rezodb; 

CREATE TABLE item( 
  item_id int primary key NOT NULL AUTO_INCREMENT
  , item_name VARCHAR (256) not null
  , item_price int not null
  , category_id int not null
);

